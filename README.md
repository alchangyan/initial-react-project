# Dependencies

+ nodejs ^8.x.x
+ npm ^5.x.x

# Run On Development
  
  ```shell
  $ git clone https://github.com/winesports/portal.git
  $ cd portal
  $ npm install
  $ npm start
  ```
# Run On Production
  
  ```shell
  $ git clone https://github.com/winesports/portal.git
  $ cd portal
  $ npm install
  $ npm run build
  ```

# Notes

+ use `import './styles.css'` over `import './styles.less' `
+ after start, *Less* will compiled automatically, so, in some cases you need to compile files manually that contains `@import` of automatically compiled file. Just run - `$ npm run build-css`