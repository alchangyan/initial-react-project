import config from '../config/default.json';
import request from './request';

export default {
  template(param1) {
    const url = `${config.template.host}?total=${param1}`;
    return request('get', url);
  },

};
