import axios from 'axios';
import jsrsasign from 'jsrsasign';
import config from '../config/default.json';
// import {put} from "redux-saga/effects";

let tokenPromise;

function* sendRequest(method, url, data, headers) {
  try {
    if (method === 'get' || method === 'delete') {
      return yield axios[method](url, {headers});
    } else {
      return yield axios[method](url, data, {headers});
    }
  } catch (err) {
    throw err;
  }
}

/*
if (Object.keys(data).length > 0) {
  url += '?'
  let QSArray = [];

  for (let key in data) {
    QSArray.push(`${key}=${data[key]}`)
  }

  url += QSArray.join('&');
}
*/

function* requestHandler(method, url, options = {data: {}, headers: {}, auth: false}) {
  
  const {data, headers, auth} = options;

  let isExpired = false;
  const AT = localStorage.getItem('AT');

  if (AT && auth) {
    const expTime = localStorage.getItem('ATET');
    const now = parseInt(new Date().getTime() / 1000, 10);

    if ((expTime - now) > 60) {
      isExpired = false;
    } else {
      isExpired = true;
    }

    if (!headers.Authorization) {
      headers.Authorization = 'Bearer ' + AT;
    }
  }

  if (!isExpired) {
    return yield sendRequest(method, url, data, headers);
  }

  const refreshToken = localStorage.getItem('RT');

  
  if (!refreshToken) {
    yield signOut();
    return;
  }

  try {
    if (!tokenPromise) {
      tokenPromise = axios.post(config.template.host + config.template.refreshToken, {refreshToken})
        .then((response) => {
          const { data: refreshTokenResponse } = response;

          if (!refreshTokenResponse.accessToken) {
            throw new Error({response: {status: 401}});
          }

          const newAccessToken = refreshTokenResponse.accessToken;
          window.localStorage.setItem('AT', newAccessToken);

          const newRefreshToken = refreshTokenResponse.refreshToken;
          window.localStorage.setItem('RT', newRefreshToken);

          const exp = jsrsasign.KJUR.jws.JWS.parse(newAccessToken).payloadObj.exp;
          window.localStorage.setItem('ATET', exp);

          tokenPromise = null;
        }).catch((err) => {
          tokenPromise = null;

          throw err;
        });
    }

    yield tokenPromise;

    headers.Authorization = 'Bearer ' + window.localStorage.getItem('AT');

  } catch (err) {
    yield signOut();
    return;
  }

  return yield sendRequest(method, url, data, headers);
}

// this function must be a generator function
function signOut() {
  console.log('sign out')
}

export default requestHandler;

