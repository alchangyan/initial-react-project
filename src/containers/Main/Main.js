import React, { PureComponent } from 'react';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';

import './styles/Main.css';

class Main extends PureComponent {
  constructor() {
    super();

    this.state = {}
  }
  
  componentWillReceiveProps(nextProps) {
  }

  componentWillMount() {
  }

  render() {
    return (
      <div className="Main">
        <Helmet>
          <title>Title</title>
          <meta name="description" content="description"/>
          <meta name="keywords" content="keywords" />
        </Helmet>
        <div>Content</div>
      </div>
    );
  }
}

Main.defaultProps = {
}

const mapStateToProps = state => {
  return {}
};

const mapDispatchToProps = dispatch => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps )(Main);
