import {
  TEMPLATE_SUCCESS,
  TEMPLATE_FAILURE,
} from '../actions/actionTypes';

const INITIAL_STATE = {
  data: [],
  fetching: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log("reducer", action)
  switch (action.type) {
    case TEMPLATE_SUCCESS:
      return {
        ...state,
        data: action.data,
        fetching: true
      };
    case TEMPLATE_FAILURE:
      return {
        ...state,
        data: INITIAL_STATE.data,
        fetching: false
      };
    default:
      return state;
  }
};
