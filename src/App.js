import React, { Component, Fragment } from 'react';
// import PropTypes from 'prop-types';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
/* Google Analytics */
import ReactGA from 'react-ga';
import { connect } from 'react-redux';
import Main from './containers/Main';
import './styles/App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {}

    this.lastLocation = '';
  }

  componentWillReceiveProps(nextProps) {

    this.props.history.listen(location => {
      if (this.lastLocation !== location.pathname) {
        this.lastLocation = location.pathname;

        /**
        * spying to location changes
        * if location is changed, then page scrolling to top
        */
        window.scrollTo(0, 0);

        ReactGA.pageview(location.pathname);
      }
    });

  }

  componentWillMount() {
  }

  render() {
    // const loggedIn = window.localStorage.getItem('AT') ? true : false;

    return (
      <Fragment>
        <div className="App">
          <div>
            <Switch>
              {/* Main Page */}
              <Route exact path="/" render={() => {
                return <Main gameId={null}/>
              }}/>

              <Redirect push to="/" />
            </Switch>
          </div>
        </div>
      </Fragment>
    );
  }
}


App.propTypes = {}

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps )(App));