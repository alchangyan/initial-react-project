import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './styles/Template.css';

class Template extends PureComponent {
  render() {
    return (
      <div className="Template">
        Template Component
      </div>
      )
  }
}

Template.defaultProps = {
  
}

Template.propTypes = {
  // name: PropTypes.string
};

export default Template;
