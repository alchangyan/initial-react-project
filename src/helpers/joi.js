import Joi from 'joi-browser';

const config = {
  passwordRegExp: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})',
};

const errorMsgs = {
  'any.empty': 'this field is required',
  'string.email': 'email is invalid',
  'string.regex.name': 'password is weak or contains incorrect symbols',
  'number.base': 'ths field must contain numeric value',
}

const schema = {
  registration: {
    nickname: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().regex(new RegExp(config.passwordRegExp), 'valid password').required(),
    passwordConfirm: Joi.string().regex(new RegExp(config.passwordRegExp), 'valid password').required(),
  },
  authorization: {
    email: Joi.string().email().required(),
    password: Joi.string().regex(new RegExp(config.passwordRegExp), 'valid password').required(),
    remember: Joi.boolean(),
  },
  userAccount: {
    nickname: Joi.string().required(),
    phone: Joi.number().empty(''),
    firstName: Joi.string().empty(''),
    lastName: Joi.string().empty(''),
    dob: Joi.number(),
    countryCode: Joi.string().empty(''),
    stateCode: Joi.string().empty(''),
    street: Joi.string().empty(''),
    city: Joi.string().empty(''),
    zip: Joi.number().empty(''),
    timezone: Joi.string().empty(''),
  },
  resetPassword: {
    password: Joi.string().regex(new RegExp(config.passwordRegExp), 'valid password').required(),
    passwordConfirm: Joi.string().regex(new RegExp(config.passwordRegExp), 'valid password').required(),
  },
  forgotPassword: {
    email: Joi.string().email().required(),
  }
};

export function validate(data, schemaType) {
  const res = Joi.validate(data, schema[schemaType], {
    abortEarly: false,
  });

  if (!res.error) {
    return {};
  }

  const { error } = res; 
  // console.log(res);

  if (error.details.length) {
    return error.details.reduce((obj, err) => {

      if (!errorMsgs[err.type]) {
        console.log('error message not found', err.type)
      }

      obj[err.path] = errorMsgs[err.type] ? errorMsgs[err.type] : err.message;
      return obj;
    }, {});
  }

  return {};
}

export function getDifference(state, oldValues) {
  let result = {};

  for (let key in state) {
    if (state[key] !== oldValues[key]) {
      result[key] = state[key];
    }
  }

  return result;
}


/**
* USAGE
*
* this.validations = validate(data, schema);
*/
