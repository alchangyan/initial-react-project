import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter} from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducers/index';
import rootSaga from './sagas';
import App from './App';

import './styles/normalize.css';
import './styles/font-awesome.css';
import './styles/index.css';

// Google Analytics
// import ReactGA from 'react-ga';

// ReactGA.initialize('UA-125959329-1');

const sagaMiddleware = createSagaMiddleware();
/*
second line in createStore function is for Redux DevTools
*/
const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);



ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>
  , document.getElementById('root'));
