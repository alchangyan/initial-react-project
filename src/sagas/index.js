import { fork, all } from 'redux-saga/effects';
import 'babel-polyfill';
import {watchTemplate} from './template';

export default function* root() {
  yield all([
    fork(watchTemplate),
  ]);
}
