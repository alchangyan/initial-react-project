import {call, put, takeEvery} from "redux-saga/effects";
import api from "../services/template-api";
import {
  TEMPLATE,
} from "../actions/actionTypes";
import {
  templateSuccess,
  templateFailure,
} from "../actions/template";

function* template(action) {
  try {
    const { data } = yield call(api.template, action.param1);
    
    yield put(templateSuccess({
      data: data.data
    }));
  } catch(error) {
    console.log(error)
    yield put(templateFailure(error));
  }
}

export function* watchTemplate() {
  yield takeEvery(TEMPLATE, template);
}
