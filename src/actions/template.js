import {
  TEMPLATE,
  TEMPLATE_SUCCESS,
  TEMPLATE_FAILURE,
} from "../actions/actionTypes";

export const template = param1 => {
  return {
    type: TEMPLATE,
    param1
  };
};

export const templateSuccess = response => {
  return {
    type: TEMPLATE_SUCCESS,
    ...response,
  };
};

export const templateFailure = error => {
  return {
    type: TEMPLATE_FAILURE,
    error
  };
};
