require('dotenv').load();
const config = require('config');
const fse = require('fs-extra');
const path = require('path');

try {
  const configFile = path.join(__dirname, '../../src/config/default.json');
  const content = config.get('content');

  fse.writeJSON(configFile, content, (err) => {
    if (err) {
      console.log(err);
    }

    console.log(`Config generated ${configFile}`);
  });
} catch (err) {
  console.log('Error while generating config', err.message);
  process.exit(1)
}
